log = e => console.log(e)
$ = e => document.getElementById(e)

function getFromValue(form) {
    var formData = new FormData();
    Array.from(form.elements).forEach(e => {
        if (e.name !== '') {
            formData.append(e.name, e.value)
        }
    })
    return formData
}

function openSnack(message, config) {
    let cfg = {
        duration: 3000,
        classes: 'color-info',
        ...config
    }
    let snack = $('snack')
    snack.innerHTML = `<p>${message}</p>`
    snack.className = `show ${cfg.classes}`;
    setTimeout(() => {
        snack.className = snack.className.replace(`show ${cfg.classes}`, "hidden");
    }, cfg.duration);
}

function openAddAllergensModal(food) {
    window.currentAllergenModalOpen = food
    $('allergensModalLabel').innerText = food.Name
    resetAllergenModal(food)
    $('openAllergenModal').click()

}

function renderAllergens(foodArray, elementId) {
    let html = [];
    $(elementId).innerHTML = ''
    foodArray.forEach(e => {
        let div = document.createElement('div')
        div.className = 'card m-3 col-md-3 col-12'
        div.style.textAlign = 'center'
        div.innerHTML = `  
        <div class="card-body">
            <p>${e.Name}</p>
        </div>`
        $(elementId).appendChild(div);
    })
}

function renderFood(foodArray, elementId) {
    let html = [];
    $(elementId).innerHTML = ''
    foodArray.forEach(e => {
        let div = document.createElement('div')
        div.className = 'card m-3 col-md-3 col-12'
        div.style.textAlign = 'center'
        div.innerHTML = `  
        <div class="card-body">
            <p>${e.Name}</p>
            <button class="btn btn-primary" onclick='openAddAllergensModal(${JSON.stringify(e)})'>Add allergens</button>
        </div>`
        $(elementId).appendChild(div);
    })
}

function updateFood() {
    http.get('/foods.php').then(data => {
        renderFood(data, 'foodAreea')
    })
}

function updateAllergens() {
    http.get('/allergens.php').then(data => {
        renderAllergens(data, 'allergensAreea')
    })
}

class HttpService {
    static apiUrl = `${window.location.protocol}/index/api`
    post(endpoint, data, options) {
        return new Promise((resolve, reject) => {
            fetch(`${HttpService.apiUrl}${endpoint}`, {
                method: 'POST',
                body: data
            }).then(response => {
                if (response.ok) {
                    return resolve(response.json())
                }
                response.json().then(data => {
                    console.error(data)
                    openSnack(data.statusMessage, {
                        classes: 'color-warn'
                    })
                })
            })
        })
    }

    get(endpoint, options) {
        return new Promise((resolve, reject) => {
            fetch(`${HttpService.apiUrl}${endpoint}`, {
                method: 'GET',
            }).then(response => {
                // log(response)
                if (response.ok) {
                    return resolve(response.json())
                }
                response.json().then(data => {
                    console.error(data)
                    openSnack(data.statusMessage, {
                        classes: 'color-warn'
                    })
                })
            })
                .catch(this.errorHandler);
        })
    }
}

http = new HttpService();