<?php
include("./config/dbContext.php");
include("./allergens/createAllergen.php");
include("./allergens/getAllAllergens.php");
include("./allergens/getAllAllergensByFood.php");
include("./config/config.php");

$db = new dbContext();
$connection = $db->getConnstring();
$request_method = $_SERVER["REQUEST_METHOD"];

switch ($request_method) {
    case 'GET':
        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);
        if (array_key_exists('Id', $queries)) {
            getAllAllergensByFood($queries['Id']);
        } else {
            getAllAllergens();
        }
        break;
    case 'POST':
        insertAllergen();
        break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}
