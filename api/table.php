<?php
include("./config/dbContext.php");
include("./config/config.php");

$db = new dbContext();
$connection = $db->getConnstring();
$request_method = $_SERVER["REQUEST_METHOD"];


switch ($request_method) {
    case 'GET':
        $arr = $_REQUEST;
        $query = "SELECT `food`.`Name` as `food`, `allergen`.`Name` as `allergen` from `food` LEFT JOIN `foodallergens` ON food.Id = foodallergens.FoodId LEFT JOIN allergen ON `allergen`.Id = foodallergens.AllergenId";
        $result = mysqli_query($connection, $query);
        $data["food"] = array();
        $foodRows = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $data["food"][] = $row;
            $foodRows[$row['food']][] = $row['allergen'];
        }

        $table = '<table class="table table-dark"><thead><tr>';
        $table .= '<th scope="col">#</th>';
        $table .= '<th scope="col">Name</th>';

        $allergens = array();
        $data["allergens"] = array();
        $query = "SELECT `allergen`.`Name` as `allergen`, Id from allergen";
        $result = mysqli_query($connection, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $data["allergens"][] = $row;
            $table .= '<th scope="col">' . $row['allergen'] . ' </th>';

            array_push($allergens, $row['allergen']);
        }
        $table .= '</tr></thead><tbody>';

        $index = 0;
        foreach (array_keys($foodRows) as $value) {
            $table .= '<tr><th scope="row">' . $index++ . '</th>';
            $table .= '<td>' . $value . '</td>';

            foreach ($allergens as $allergen) {
                $table .= '<td>' . (in_array($allergen, $foodRows[$value]) == 1 ? "X" : "") . '</td>';
            }
            $table .= '</tr>';
        }
        $table .= '</tbody></table>';

        http_response_code(200);
        header('Content-Type: application/json');
        echo json_encode($table);
        break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}
