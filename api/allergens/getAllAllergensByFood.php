<?php
function getAllAllergensByFood($Id)
{
    global $connection;


    $query = "SELECT `allergen`.`Name` FROM foodallergens INNER JOIN allergen ON `allergen`.`Id` = `foodallergens`.`AllergenId` WHERE `foodallergens`.`FoodId` = " . $Id;
    $result = mysqli_query($connection, $query);
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }
    http_response_code(200);
    header('Content-Type: application/json');
    echo json_encode($data);
}
