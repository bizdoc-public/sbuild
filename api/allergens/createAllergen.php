<?php
function insertAllergen()
{
    global $connection;

    $arr = $_REQUEST;
    $values = "";

    foreach ($arr as $key => $value) {
        $values .= ucfirst($key) . " = '{$value}', ";
    }

    $values = substr_replace($values, ";", -2);

    $query = "INSERT INTO allergen SET $values";

    if (mysqli_query($connection, $query)) {
        http_response_code(201);
        $response = array(
            'statusMessage' => 'Allergen Added Successfully.'
        );
        echo json_encode($response);
    } else {
        http_response_code(400);
        $response = array(
            'statusMessage' => $connection->error
        );
        echo json_encode($response);
    }
}
