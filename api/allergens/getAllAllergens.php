<?php

function getAllAllergens()
{
    global $connection;

    $arr = $_REQUEST;
    $query = "SELECT * FROM allergen;";
    $result = mysqli_query($connection, $query);
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }
    http_response_code(200);
    header('Content-Type: application/json');
    echo json_encode($data);
}
