<?php
include("./config/dbContext.php");
include("./foods/createFood.php");
include("./foods/getAllFoods.php");
include("./foods/addAllergensToFood.php");
include("./config/config.php");

$db = new dbContext();
$connection = $db->getConnstring();
$request_method = $_SERVER["REQUEST_METHOD"];


switch ($request_method) {
    case 'GET':
        getAllFoods();
        break;
    case 'POST':
        $queries = array();
        parse_str($_SERVER['QUERY_STRING'], $queries);
        if (array_key_exists('Id', $queries)) {
            addAllergens($queries['Id']);
        } else {
            insertFood();
        }
        break;
    default:
        header("HTTP/1.0 405 Method Not Allowed");
        break;
}
