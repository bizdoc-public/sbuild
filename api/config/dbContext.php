<?php

class dbContext
{
    var $servername;
    var $username;
    var $password;
    var $dbname;
    var $conn;

    function __construct()
    {
        $this->servername = getenv('DBSERVER') ? getenv('DBSERVER') : "localhost";
        $this->username = getenv('DBUSER') ? getenv('DBUSER') : "root";
        $this->password = getenv('DBPASSWORD') ? getenv('DBPASSWORD') : "";
        $this->dbname = getenv('DBNAME') ? getenv('DBNAME') : "softBuild";
    }

    function getConnstring()
    {
        $con = mysqli_connect($this->servername, $this->username, $this->password, $this->dbname) or die("Connection failed: " . mysqli_connect_error());

        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        } else {
            $this->conn = $con;
        }
        return $this->conn;
    }
}
