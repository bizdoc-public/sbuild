<?php

function getAllFoods()
{
    global $connection;

    $arr = $_REQUEST;
    $query = "SELECT * FROM food;";
    $result = mysqli_query($connection, $query);
    $data = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $data[] = $row;
    }
    http_response_code(200);
    header('Content-Type: application/json');
    echo json_encode($data);
}
