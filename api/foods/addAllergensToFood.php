<?php
function addAllergens($id)
{
    global $connection;

    $arr = $_REQUEST;

    $request_body = file_get_contents('php://input');
    $data = json_decode($request_body);
    $count = 0;
    foreach ($data as $value) {
        $query = 'INSERT INTO `foodallergens`(`FoodId`, `AllergenId`) VALUES (' . $id . ',' . $value . ')';
        if (mysqli_query($connection, $query)) {
            $count++;
        }
    }

    http_response_code(201);
    $response = array(
        'statusMessage' => $count . " allergens added"
    );
    echo json_encode($response);
}
